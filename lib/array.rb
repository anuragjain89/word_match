class Array
  def find_words_containing(pattern)
    regex = Regexp.new(Regexp.escape("#{ pattern }"), Regexp::IGNORECASE)
    select { |element| element =~ regex }
  end
end
